---
templateKey: index-page
title: Great roses with a conscience
image: /img/cotopaxi.jpg
heading: Ecuadorian Roses + More
subheading: Serving IE and LA County
mainpitch:
  title: Why United Flower Wholesale
  description: >
    UFW is the flower wholesaler for everyone who believes that great roses
    shouldn't just smell good, they should do good too. We source all of our roses
    directly from our own farm and those of small scale sustainable farmers in Ecuador to make sure profits
    profits are reinvested in their communities.
description: >-
  United Flower Wholesale is a family-owned business specializing in importing directly from the Cotopaxi volcanic region in Ecuador.
intro:
  blurbs:
    - image: /img/coffee.png
      text: >
        We sell flowers
    - image: /img/coffee-gear.png
      text: >
        Like lots of them
    - image: /img/tutorials.png
      text: >
       You'd really like them
    - image: /img/meeting-space.png
      text: >
        Buy them for the ones you love
  heading: What we offer
  description: >
    Tons, I reckon
main:
  heading: Great flowers with no compromises
  description: >
    We hold our roses to the highest standards from the farm to the vase.
    We personally run our farm and work closely with farms in the region to make sure the conditions are
    optimal for the plants, farmers and the local environment.
  image1:
    alt: A close-up of a paper filter filled with ground coffee
    image: /img/products-grid3.jpg
  image2:
    alt: A green cup of a coffee on a wooden table
    image: /img/products-grid2.jpg
  image3:
    alt: Coffee beans
    image: /img/products-grid1.jpg
---
